#!/usr/bin/python
def printRoute(s,route):
    print s
    for i in range(4):
        print '\t',
        for j in range(4):print route[i][j],'  ',
        print

def initGrid():
    return [['.' for i in range(4)] for j in range(4)]

def constraints(func):
    def inrange(obj,x,y,d,idx,s,route):
        if x>=0 and x<4 and y>=0 and y<4 and route[x][y]=='.':
            idx = obj.trie.seek(idx,obj.grid[x][y])
            if idx is not None:return
            func(obj,x,y,d,idx,s,route)
    return inrange

class Trie:
    def __init__(self):
        self.len = 0
        self.obj = [{}]
    def insert(self,s):
        idx = 0
        for i in s:
            if not self.obj[idx].has_key(i):
                self.obj.append({})
                self.len+=1
                self.obj[idx][i] = self.len
            idx = self.obj[idx][i]
        self.obj[idx]['$'] = 1
    def seek(self,idx,c):
        for i in c:
            if self.obj[idx].has_key(i):
                idx = self.obj[idx][i]
            else:return
        return idx
    def over(self,idx):
        return self.obj[idx].has_key('$')

class Wordament:
    def __init__(self,grid,trie):
        self.trie=trie
        self.grid=grid
        self.words={}
        self.maxdepth=0

    @constraints
    def traverse(self,i,j,d,idx,s,route):
		s=s+obj.grid[i][j]
		route[i][j]=d+1
        if d==self.maxdepth:
            if self.trie.over(idx) and not self.words.has_key(s):
                printRoute(s,route)
                self.words[s]=1
        else:
            nextx=[1,1,-1,-1,0,0,1,-1]
            nexty=[1,-1,1,-1,1,-1,0,0]
            for k in range(8):
                self.traverse(i+nextx[k],j+nexty[k],d+1,idx,s,route)
		route[i][j]='.'

    def solve(self):
        for self.maxdepth in range(2,17):
            for i in range(4):
                for j in range(4):
                    routes=initGrid()
                    self.traverse(i,j,0,0,"",routes)

trie = Trie()
with open("words.txt","r") as w:
    for i in w:
        trie.insert(i.upper()[:-2])

with open("test","r") as w:
    s=[]
    for i in w:
        s.append(i.upper().split())
    instance = Wordament(s,trie)
    instance.solve()
    print len(instance.words.keys()),"words"