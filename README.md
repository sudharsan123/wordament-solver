##Wordament Solver

+ Specify the input for the script through the test file. 

+ The script works for grids that have multiple characters too. 

+ Certain obscure words will not be found with this script. However on average you can generate around 100-200 words for the grids provided by the Wordament app

+ If you want more words, then change the ```words.txt``` file accordingly. Currently it has around 80000 words. 

####TODO:
1. Providing support for prefix and suffix grids like ```-ing```, ```-ed```, etc.
2. Resolving ```\r\n``` issues on windows
